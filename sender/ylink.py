import urllib
import urllib2
import json
import requests
import base64

def getAuth():
    query_args = { 'grant_type':'client_credentials', 'scope':'default' }

    request = urllib2.Request('https://www.livepaperapi.com/auth/v1/token')
    request.add_data(urllib.urlencode(query_args))
    request.add_header('Authorization','Basic dmc2OXlmcDJtYWRmenhtaW8xNzhkdmlxdXozcXY1b286VU41Vk1XV3Blb1lYZFplQWE4SGpVSXFSN1ZTZ3N2WHY=')
    request.add_header('Content-Type','application/x-www-form-urlencoded')
    request.add_header('Accept','application/json')
    request.get_data()
    return urllib2.urlopen(request).read()

def createTrigger(access_token):
    request = urllib2.Request('https://www.livepaperapi.com/api/v1/triggers')
    request.add_header('Content-Type','application/json')
    request.add_header('Accept','application/json')
    request.add_header('Authorization', 'Bearer ' + str(access_token))
   
    trigger_data = {'name':'MyWatermarkTrigger', 'type':'watermark'}    
    query_args = {'trigger':trigger_data}
    query_encoded = json.dumps(query_args)
    request.add_data(query_encoded)
    request.get_data()
    return json.loads(urllib2.urlopen(request).read())

def createPayoff(access_token, payURL):
    request = urllib2.Request('https://www.livepaperapi.com/api/v1/payoffs')
    request.add_header('Content-Type','application/json')
    request.add_header('Accept','application/json')
    request.add_header('Authorization', 'Bearer ' + str(access_token))
    rich_data = {"type":'content action layout', 'version':1,'data':{'content':{"type":"video", 'label':'YOUTUBE VIDEO', 'data':{'URL':payURL, 'imageURL':'img.youtube.com/vi/y9sJQs2MdSc/mqdefault.jpg','fullscreen':False, 'autoplay': True}, 'actions':[]}}}
    json_rich = json.dumps(rich_data)
    encoded_rich = base64.b64encode(json_rich)
    payoff_data = {'name':'myPayoff', 'richPayoff':{'version':'1.0', 'private':{'content-type':'custom-base64', 'data':encoded_rich}, 'public':{'url':payURL}}}
    query_args = {'payoff':payoff_data}
    query_encoded = json.dumps(query_args)
    request.add_data(query_encoded)
    request.get_data()
    return json.loads(urllib2.urlopen(request).read())

def createLink(access_token, trig_id, pay_id):
    request = urllib2.Request('https://www.livepaperapi.com/api/v1/links')
    request.add_header('Content-Type','application/json')
    request.add_header('Accept','application/json')
    request.add_header('Authorization', 'Bearer ' + str(access_token))
    link_data = {'name':'myLink', 'payoffId':str(pay_id), 'triggerId':str(trig_id)}
    query_args = {'link':link_data}
    query_encoded = json.dumps(query_args)
    request.add_data(query_encoded)
    request.get_data()
    return json.loads(urllib2.urlopen(request).read())

def getImageBinary(url):
    request = urllib2.Request(url)
    request.add_header('Accept', 'image/jpeg')
    request.get_data()
    return urllib2.urlopen(request).read()

def postImage(access_token, url):
    request = urllib2.Request('https://storage.livepaperapi.com/objects/v1/files')
    request.add_header('Content-Type','image/jpeg')
    request.add_header('Authorization', 'Bearer ' + str(access_token))
    binary_data = getImageBinary(url)
    request.add_data(binary_data)
    request.get_data()
    return urllib2.urlopen(request).read()

def postImageRe(access_token, binary_data):
    res = requests.post(url='https://storage.livepaperapi.com/objects/v1/files',
                        data=binary_data,
                        headers={'Content-Type': 'image/jpeg',
                                 'Authorization': 'Bearer ' + str(access_token),
                                 'Accept': 'application/json'})
    
    print res.headers
    return res.headers['location']

def getImageURL(access_token, src_img_url, wm_url):
    full_url = wm_url + '?imageUrl=' + src_img_url + '&resolution=75&strength=10'
    return full_url
    
def do():
    auth_res = json.loads(getAuth())
    access_token = auth_res['accessToken']
    triggerRes = createTrigger(access_token)
    payoffRes = createPayoff(access_token)
    trigger_id = triggerRes['trigger']['id']
    payoff_id = payoffRes['payoff']['id']
    createLink(access_token, trigger_id, payoff_id)
    return

def addYouTubeCover(youtubeURL, binaryDat):
    auth_res = json.loads(getAuth())
    access_token = auth_res['accessToken']
    triggerRes = createTrigger(access_token)
    trigger_id = triggerRes['trigger']['id']
    wm_url = triggerRes['trigger']['link'][0]['href']
    payoffRes = createPayoff(access_token, youtubeURL)
    payoff_id = payoffRes['payoff']['id']
    createLink(access_token, trigger_id, payoff_id)
    src_img_url = postImageRe(access_token, binaryDat)
    full_url = getImageURL(access_token, src_img_url, wm_url)
    resultDict = {'token':'Bearer '+access_token, 'url':full_url}
    return resultDict
    
#vidURL = 'https://www.youtube.com/watch?v=nFSQExbSFKw'
#url = 'https://i.ytimg.com/vi/4YgKeERVhhc/mqdefault.jpg'
#binary_data = getImageBinary(url)
#print addYouTubeCover(vidURL, binary_data)
#print postImageRe(access_token,url)

