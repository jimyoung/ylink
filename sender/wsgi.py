#!/usr/bin/env python

import os
import bottle
import logging

from bottle import route, request,post, template
import  pika
import threading
from time import sleep
import json
import psutil
import ylink
import tester

STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static')

logging.basicConfig(format="%(threadName)s:%(thread)d:%(message)s")
log = logging.getLogger('sender')
log.setLevel(logging.DEBUG)

log.debug("setting up message queue")

rabbit_url = os.environ['RABBITMQ_URL']
queue_name = os.environ['QUEUE_NAME']
async_cpu = os.environ['ASYNC_CPU']

log.debug("rabbit mq url:%s"%os.environ['RABBITMQ_URL'])

def asyncCPUTest():
	log.debug("starting async CPU test method")
	while (1):
		sleep(5)
		log.debug("CPU percent = %d"%(psutil.cpu_percent()))
	
	
if (async_cpu != None) and async_cpu == "1":
	keepGoing = True
		
	"""
	sets up the async message processing thread, passing it the queue name to listen on
	"""
	log.debug("spawning async CPU loadtest thread")
	d = threading.Thread(name='sender-daemon', target=asyncCPUTest, args = ())
	d.setDaemon(True)
	d.start()



'''
view routes
'''
@post('/ylink')
def ylinkMethod():
    youtubeURL = request.POST.get('youtube_url')
    imageURL = request.POST.get('image_url')
    #imageURL = 'https://yt3.ggpht.com/-x3CU1CXklQI/AAAAAAAAAAI/AAAAAAAAAAA/jPQ9GJeU53g/s88-c-k-no/photo.jpg'
    binaryDat = ylink.getImageBinary(imageURL)
    resultT = ylink.addYouTubeCover(youtubeURL, binaryDat)
    return resultT
    #return imageURLa

@route('/test')
def hello():
    return "hello"

@route('/')
def home():
	bottle.TEMPLATE_PATH.insert(0, './sender/views')
	return bottle.template('home', sent=False, body=None)

'''
service runner code
'''
log.debug("starting web server")
application = bottle.app()
application.catchall = False

"""
#UNCOMMENT BELOW FOR RUNNING ON LOCALHOST
if os.getenv('SELFHOST', False):

url = os.getenv('VCAP_APP_HOST')
port = int(os.getenv('VCAP_APP_PORT'))
bottle.run(application, host=url, port=port)

#UNCOMMENT BELOW FOR RUNNING ON HDP
"""

bottle.run(application, host='0.0.0.0', port=os.getenv('PORT', 8080), debug=True )


# this is the last line
